<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', ['middleware' => ['api']], function ($api) {
    /**
     * Authorization
     */
    $api->group(['prefix' => 'auth'], function ($api) {
        $api->post('token', 'App\Http\Controllers\AuthController@token');
    });

    $api->resource('users', 'App\Http\Controllers\Api\UserController');
});
