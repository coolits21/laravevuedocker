<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        
        // Apply role
        $user->addRole('Admin');

        $user->name = Str::random(10);
        $user->email = "amalankar@gmail.com";
        $user->password = Hash::make('1234qwer');
        $user->save();
    }
}
