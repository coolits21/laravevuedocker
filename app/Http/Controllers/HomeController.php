<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Where user is redirected when visiting homepage
     *
     * @return view app
     */
    public function index()
    {
        return view('app');
    }

    public function test()
    {
        dd('hello world');
    }
}
