<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Transformers\UserTransformer;
use App\Repositories\UserRepository;
use App\Http\Controllers\ApiBaseController;

class UserController extends ApiBaseController
{

    public function __construct(
        UserTransformer $userTransformer,
        UserRepository $userRepository
    ) {
        $this->repository = $userRepository;
        $this->transformer = $userTransformer;
    }

    // public function index(Request $request)
    // {
    //     dd('test');
    // }
}
