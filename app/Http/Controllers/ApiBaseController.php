<?php

namespace App\Http\Controllers;

use App\Traits\Http\Response;
use Dingo\Api\Routing\Helpers;
use App\Repositories\Criterias\PaginationCriteria;
use Illuminate\Http\Request;

class ApiBaseController extends Controller
{
    use Response, Helpers;

    public function index(Request $request)
    {
        $page = $request->get('page');

        if (is_null($page)) {
            $page = [
                'size' => 10,
                'index' => 0,
            ];
        }

        $this->repository->pushCriteria(new PaginationCriteria($page));

        if ($page['size'] > 0) {
            $url = str_replace('api/', '', $request->path());
            $results = $this->repository
                            ->paginate($page['size'])
                            ->withPath($url);
        } else {
            $results = $this->repository->all();
        }

        return $this->success($results);
    }

    /**
     * checkIncludeParameters function
     *
     * @param [type] $includeParameters
     * @return boolean
     */
    public function checkIncludeParameters($includeParameters)
    {
        
        $avaialbleIncludes = $this->transformer->getAvailableIncludes();
        //explode requested string
        $explodedRequestIncludes = explode(',', $includeParameters);

        foreach ($explodedRequestIncludes as $value) {
            //check if item is in available includes
            if (!in_array(camel_case($value), $avaialbleIncludes)) {
                return false;
            }
        }

        return true;
    }
}
