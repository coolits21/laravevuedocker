import axios from "axios";
import Vue from 'vue'

const devInstance = createInstance("http://laravelvuecore.local/");
const productionInstance = createInstance("http://localhost:3000"); // will change later

function createInstance(baseURL){
    return axios.create({
        baseURL,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('jwt')}`
        }
    });
}

export default {
    install () {
        Vue.prototype.$http = devInstance
    }
};