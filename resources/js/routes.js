import Vue from 'vue';
import VueRouter from 'vue-router';

import Dashboard from '@/js/components/Dashboard';
import Login from '@/js/components/Login';
import Users from '@/js/components/Users';
import Configuration from '@/js/components/Configuration';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: { 
                guest: true,
                layout: 'no-sidebar-layout'
            }
        },
        {
            path: '/',
            name: 'dashboard',
            component: Dashboard,
            meta: { 
                requiresAuth: true,
                layout: 'default-layout'
            }
        },
        {
            path: '/users',
            name: 'users',
            component: Users,
            meta: { 
                requiresAuth: true,
                layout: 'default-layout'
            }
        },
        {
            path: '/configuration',
            name: 'configuration',
            component: Configuration,
            meta: { 
                requiresAuth: true,
                layout: 'default-layout'
            }
        }
    ]
});

router.beforeEach((to, from, next) => {
    if(to.matched.some(record => record.meta.requiresAuth)) {
        if (localStorage.getItem('jwt') == null) {
            next({
                path: '/login',
                params: { nextUrl: to.fullPath }
            })
        } else {
            let user = JSON.parse(localStorage.getItem('user'))
            if(to.matched.some(record => record.meta.is_admin)) {
                if(user.is_admin == 1){
                    next()
                }
                else{
                    next({ name: 'dashboard'})
                }
            }else {
                next()
            }
        }
    } else if(to.matched.some(record => record.meta.guest)) {
        if(localStorage.getItem('jwt') == null){
            next()
        }
        else{
            next({ name: 'dashboard'})
        }
    }else {
        next() 
    }
});

export default router;