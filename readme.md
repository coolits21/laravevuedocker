
## Project Installation

- clone this repo to your local
- run composer install - this is for laravel vendor packages
- run npm install - this is for vuejs node_module
- update your .env file adding 
API_DOMAIN=add your local domain here
API_STANDARDS_TREE=x
API_SUBTYPE=core
API_PREFIX=api
API_VERSION=v1
API_DEBUG=true
API_STRICT=false
- run php artisan jwt:secret - this is to generate jwt key to be used in token
- run php artisan migrate